package edu.khai.practice.travelagency.service;

public interface UserService {

  boolean authorize(String email, String password);
}
